### git 配置

###### 查看所有配置及所在的文件

```shell
git config --list --show-origin
```

###### 查看全局配置

```
git config --global --list 
```

###### 设置全局用户名和邮箱

```shell
 # --global 全局信息，只需要设置一次
 git config --global user.name "duyafei"
 git config --global user.email ***@example.com
```

###### 查看本地配置

```shell
git config --local --list 
```

###### 设置本地用户名和邮想

​		项目提交时使用的用户名和邮箱，如果没有本地配置，就取全局

```shell
 git config user.name "dyf"
 git config user.email ***@example.com
```

> 仅仅是提交的是的信息， 要和github的用户邮箱区分开，没有关系的

### git 使用

### github & gitlab & gitee

许多 Git 服务器都使用 SSH 公钥进行认证。 为了向 Git 服务器提供 SSH 公钥，如果某系统用户尚未拥有密钥，必须事先为其生成一份， 默认情况下，用户的 SSH 密钥存储在其 `~/.ssh` 目录下； id_rsa.pub 公钥，id_rsa 私钥，如果 没有（或者根本没有 `.ssh` 目录），就运行 `ssh-keygen` 程序就创建一下。 

###### 查看下

```shell
@MBP.dyf ➜ ~  cd ~/.ssh
@MBP.dyf ➜ .ssh  ll
total 40
-rw-------  1 duyafei  staff   1.6K Mar  3  2019 id_rsa
-rw-r--r--  1 duyafei  staff   398B Mar  3  2019 id_rsa.pub
-rw-r--r--  1 duyafei  staff    10K May 16 11:47 known_host
```

###### 生成ssh公钥

```shell
@MBP.dyf ➜ .ssh  ssh-keygen -o    # 执行命令
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/duyafei/.ssh/id_rsa): my_rsa  #  文件名
Enter passphrase (empty for no passphrase):   # 密码，空，直接回车
Enter same passphrase again:   # 密码，空，直接回车
Your identification has been saved in my_rsa.
Your public key has been saved in my_rsa.pub.
The key fingerprint is:
SHA256:lpLtJaPDbEu+Hj9v0h1tbc+gKH9CoFTqOofrW2/09Y4 duyafei@MBP.dyf
The key's randomart image is:
+---[RSA 3072]----+
|                 |
|        .        |
|       o         |
|      oo..       |
|     oo.S..  . . |
|     oo=.+. o + o|
|     oX..+ + = +.|
|    +=.*= * +.. o|
|   .+*=ooBooE..  |
+----[SHA256]-----+
@MBP.dyf ➜ .ssh  ll
total 56
-rw-------  1 duyafei  staff   1.6K Mar  3  2019 id_rsa
-rw-r--r--  1 duyafei  staff   398B Mar  3  2019 id_rsa.pub
-rw-r--r--  1 duyafei  staff    10K May 16 11:47 known_hosts
-rw-------  1 duyafei  staff   2.5K Jun 20 17:51 my_rsa       # 新私钥
-rw-r--r--  1 duyafei  staff   569B Jun 20 17:51 my_rsa.pub   # 新公钥
```

###### 查看公钥内容

类似这么一串,这个要配置到 git服务器上，配置方式省略。。

```shell
@MBP.dyf ➜ .ssh  cat ~/.ssh/my_rsa.pub
ssh-rsa AAAAB3NzaC23423423423DdwxE8bDn60mtE4wKv/SwKZ3EUdq+iiq/V15KTOjUI/7fp0qH5Bz9R9drX82hDYurMG9PEIhvAylhcJ98sJxlEBtH/Yg/CYZRAk7lTEfShvXvHYpYOizE9ebmleA/jb8ua90U15cpYpEMho3MaHwgHq/Zqc5e0rrcADtd15X1G9YAJFNelSCVN420jJFM0zt7LIQhgJCPXd+2Y41DdoHJ+6s12xTkVW8qvh01F8flgTbJStT5PdLe7PrbJvM0R9G2FsVPI942347bOZx87ftZ6xl23423mY4O/SEMwjaCYa3gteGScBRHeoPcUa9B5vlwAUuknZAUpPbU6cYyoNPbdbwd4eqOhC2l9D5vz0UdsnSuJDRJRXUXgelQf4RoIlvQPxb3yYMpZckR/96sNpr8+nfxAtscvD8HrJi+fM5PIvD832423423423lRnDV/GQfhkHT/U7eUXZF6TKISROQXRX5GctQ8XMKujTvv+05VGgIR3VO15AL6+Wk= duyaf23423423BP.dyf
```

###### gitee 配置公钥

![]()

###### 测试

```shell
@MBP.dyf ➜ .ssh  ssh -T git@gitee.com
Hi dyfSama! You've successfully authenticated, but GITEE.COM does not provide shell access.

# clone项目 http方式
@MBP.dyf ➜ ~  git clone https://gitee.com/dyfSama/myspcloudconfig.git
Cloning into 'myspcloudconfig'...
remote: Enumerating objects: 35, done.
remote: Counting objects: 100% (35/35), done.
remote: Compressing objects: 100% (34/34), done.
remote: Total 35 (delta 15), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (35/35), 3.10 KiB | 144.00 KiB/s, done.

# clone项目 ssh方式
@MBP.dyf ➜ ~  git clone git@gitee.com:dyfSama/myspcloudconfig.git
Cloning into 'myspcloudconfig'...
remote: Enumerating objects: 35, done.
remote: Counting objects: 100% (35/35), done.
remote: Compressing objects: 100% (34/34), done.
remote: Total 35 (delta 15), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (35/35), done.
Resolving deltas: 100% (15/15), done.
```

