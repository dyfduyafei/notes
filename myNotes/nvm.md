[GitHub](https://github.com/nvm-sh/nvm)

##### nvm 安装&更新

###### 安装nvm

执行脚本, 项目下载到 ~/.nvm 下

```shell
# curl
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

# wget
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```

 成功后，会将以下的环境变量写入到以下配置文件中（(`~/.bash_profile`, `~/.zshrc`, `~/.profile`, or `~/.bashrc`）

```shell
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```

###### 检验安装

执行以下命令应该 输出 nvm

```shell
command -v nvm
```



##### nvm 使用

###### 安装node

```shell
# 最新版
nvm install node

# 稳定版
nvm install stable

# 安装指定版本node  nvm install 14.0.0 或者 nvm install v14.0.0
nvm install <version>

# /node_modules 在 node安装路径下
# 比如:  ~/.nvm/versions/node/v14.4.0/lib/node_modules
```

######卸载node

```shell
nvm uninstall <version>
```

###### 切换node版本

```shell
nvm use <version>
```

###### 其他命令

```shell
# 列出所有安装的版本
nvm ls 

# 列出所有远程服务器的版本
nvm ls-remote

# 显示当前版本
nvm current

# 给不同的版本号添加别名
nvm alias <name> <version> 

# 删除已定义的别名
nvm unalias <name> 

# 查看安装路径
nvm which <version> 
```

