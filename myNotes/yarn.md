# yarn

[TOC]



###### 官方文档 [GitHub](https://github.com/yarnpkg/yarn)

###### yarn 安装

​	macos

```shell
# 通过brew安装
brew install yarn
# 通过brew升级
brew upgrade yarn
# 安装后路径
/usr/local/Cellar/yarn/1.22.4/...
# 链接路径
/usr/local/bin/yarn -> ../Cellar/yarn/1.22.4/bin/yarn
```

 	centos

```shell
sudo yum install yarn
```

​	windows    [Download installer](https://classic.yarnpkg.com/latest.msi)

###### 查看版本

```shell
yarn -v  
yarn -version
```

######开始一个新工程

```shell
# 通过交互式会话创建一个 package.json
yarn init 

# 跳过会话，直接通过默认值生成 package.json
yarn init --yes 
yarn init -y
```

######添加依赖

```shell
# 通过 yarn add 添加依赖会更新 package.json 以及 yarn.lock 文件
yarn add [package]
yarn add [package]@[version]
yarn add [package]@[tag]
```

######以不同的策略添加依赖

```shell
yarn add [package] --dev
yarn add [package] --peer
yarn add [package] --optional
```

###### 升级依赖

```shell
yarn upgrade [package]
yarn upgrade [package]@[version]
yarn upgrade [package]@[tag]
```

###### 删除依赖

```shell
yarn remove [package]
```

###### 安装项目依赖

```shell
yarn     or   yarn install
```

###### yarn 缓存

```shell
yarn global list
# yarn依赖缓存位置
/Users/用户名/.config/yarn
# 依赖包目录
/Users/用户名/config/yarn/global/node_modules
# yarn全局安装过的包
/Users/用户名/config/yarn/global/package.json
```



