公司电脑要求安装McAfee杀毒软件，但是这个软件真的很烦

1. 会拖慢电脑速度（感觉很明显）。
2. 很大部分盗版软件用不了（破解工具被杀了），毕竟我们在天朝。
3. 就是不喜欢这个软件。

卸不掉，也不能临时禁用，就是公司认可的安全(流氓)软件。所以要暴力的卸载掉。

## 一 、手动删除

需要的工具：Everything、一个空的U盘、一双手。

Everything 下载：

打开Everything 选项选择所有，关键词搜索mcafee，结果大致是这样的，记住这些目录

![1](https://gitee.com/dyfSama/pic-bed/raw/master/md/%E6%94%BF%E5%8A%A1%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_16136160888763-G66s8d.png)

然后用U盘 做个PE系统

PE 下载：

插上U盘，打开PE工具箱

![image-20210218114211384](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210218114211384-ecbhIb.png)

选择U盘这个图标

![image-20210218114327772](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210218114327772-CvUdyJ.png)

选下U盘，其余默认，点击“立即安装进U盘”

安装完毕，重启电脑，选择PE系统，进入PE，将之前记下的目录全部删掉，然后重启系统就好了