**使用Toolbox App，安装、升级、卸载Jetbrain家族的软件。**

1. 工程和IDE 一起管理
2. 支持版本的回退，尝鲜不爽可以一键回滚。

## 1. 安装ToolboxApp

###1.1 下载

地址：https://www.jetbrains.com/toolbox-app/

选择适合自己系统的安装包下载，这里选择第一个是 **.exe(Windows)**

![image-20210207155434633](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207155434633-51QVER.png)

### 1.2 安装

安装完是这样的。

>1. Project 是项目列表，支持收藏和搜索。
>2. Tools是软件列表。
>   1. 可用的软件，总有一个适合你的。
>   2. 如果之前安装过比如pycharm，会显示手动安装的软件列表
>   3. 用toolboxapp安装的，会显示已安装的软件列表
>3. Community 是社区版，免费版同时也是功能阉割版。其余都是收费版本。

![image-20210207160000646](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207160000646-G8jvgY.png)

> **ToolboxApp 安装目录**：C:\Users\duyafei\AppData\Local\JetBrains\Toolbox

### 1.3 配置

点击右上角螺丝帽，进入配置页面，需要配置Tools选项

![image-20210207160745057](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207160745057-mhSPRR.png)

修改下Tools install location即软件安装目录，**点击Change进行修改**。

![image-20210207161128917](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207161128917-t0ZN4Z.png)

**点击Apply**，完成配置。

也可以修改语言等其他界面显示

![image-20210207172046969](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207172046969-lM55Q8.png)

## 2. 使用ToolboxApp

以Pycharm为例。

### 2.1 安装

如图，可以选择版本，这里选择最新版。

![image-20210207161428429](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207161428429-nYiA3g.png)

看你的网速了

![image-20210207161605275](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207161605275-LmvhRV.png)

等待片刻，安装完成！

>![image-20210207162348815](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207162348815-Cpg3Gc.png)

同样点击右边螺丝帽，选择setting，进入pycharm设置页面，可以做一些配置，也可以默认

1. 是否自动更新，默认否
2. 显示的名字，
3. 堆大小，默认 750M，也可以在Pycharm里面调整。
4. pycharm配置的路径，默认为空，其实是：C:\Users\duyafei\AppData\Local\JetBrains\PyCharm2020.3						

![image-20210207162804285](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207162804285-4LR4bN.png)

### 2.2 卸载

![image-20210207165116760](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207165116760-l1qR2G.png)

### 2.3 升级

按照提示更新就行，增量更新，很快。

### 2.4 回滚

![image-20210207165138184](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207165138184-8pLaUL.png)

### 2.5 工程管理

搜索、收藏（置顶）、隐藏。

![image-20210207165521904](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207165521904-Hoioqz.png)