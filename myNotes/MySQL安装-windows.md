## 1.下载

### 官网下载

官网是 https://dev.mysql.com/

![Snipaste_2021-02-07_11-21-48](https://gitee.com/dyfSama/pic-bed/raw/master/md/Snipaste_2021-02-07_11-21-48-LoDvKY.jpg)

![Snipaste_2021-02-07_11-22-04](https://gitee.com/dyfSama/pic-bed/raw/master/md/Snipaste_2021-02-07_11-22-04-N2AU0j.jpg)

选择操作系统**Windows**和**Windows (x86, 32-bit), MSI Installer**（400M这个）

>安装程序是32位的，安装的mysq产品都是64位的。

![image-20210207125606554](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207125606554-6Ibtq7.png)

点击**Download** 提示登录，不需要直接点击**No thanks, just start my download**下载

![Snipaste_2021-02-07_11-21-24](https://gitee.com/dyfSama/pic-bed/raw/master/md/Snipaste_2021-02-07_11-21-24-dsgZdv.jpg)

下载完是这样的。

<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207130343389-L2CaIO.png" alt="image-20210207130343389" style="zoom:50%;" />

> 如果下载慢的话，百度网盘：链接: https://pan.baidu.com/s/1BmebEnu4gEsWeA9Q_QYdUA  密码: 0o3h

## 2.安装和配置

### 2.1 安装

这里有五个模式，我们选择 **Server only 模式**（最小化安装）

> 1.  Developer Default: 开发者模式，和Full 一样安装了所有产品
> 2. Server only： 只安装了Mysql Server
> 3. Client only： 客户端模式
> 4. Full：和开发者模式一样，反正点进去产品列表是一样的。
> 5. Custom：自定义模式，这里可以选择**各种产品及版本（5.7，5.6)和子版本**。

![image-20210207141953890](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207141953890-pmLtR7.png)

点击next，准备安装

![image-20210207142141592](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207142141592-Cf3W6C.png)

点击 Excute，安装完成。

![image-20210207142225134](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207142225134-4I1dl3.png)

## 3.产品配置

 点击next 后， 来到产品配置界面，准备配置

![image-20210207142359958](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207142359958-NzhqT3.png)

点击Next，开始配置

### 3.1 类型和网络

默认配置即可，点击Next

![image-20210207142714126](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207142714126-Rt9D1b.png)

### 3.2 认证策略

选择第二个传统模式，点击Next

![image-20210207142841512](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207142841512-lyqCP3.png)

> 第一种密码策略更安全，但是会影响某些mysql可视化界面(比如 Navicat)连接，所以这里选择第二种。

### 3.3 账户和角色

1. 配置账户密码，上一步我们选择的第二种认证策略，所以这里不进行强密码校验，这里我的密码是123456

2. 账户可以在这里配置，也可以以后通过数据库配置，这里选择不配置，点击Next

   ![image-20210207143159270](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207143159270-NPD71s.png)

   ### 3.4 windows服务配置

   这里配置服务名（默认MySQL80）和服务开启启动策略（默认开机启动）,默认即可，点击Next

   ![image-20210207143352583](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207143352583-GU51WR.png)

### 3.4 执行配置

点击 Excuete执行配置。

![image-20210207143515854](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207143515854-GIp9rp.png)

配置完成，点击Finish

![image-20210207143611557](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207143611557-NHeNlm.png)

产品配置完成了，点击Next。

![image-20210207143642126](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207143642126-7vD9EV.png)

mysql安装完成了，点击Finish。

![image-20210207143801996](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207143801996-gpjcWj.png)

会来到这个界面，Mysql Server 安装配置完成。这个界面可以关掉了。

![image-20210207144125787](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207144125787-Cbpcr1.png)



## 4. 测试

安装完成，开始菜单会有这三个。

![image-20210207144339178](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207144339178-BlKCUg.png)

打开 **MySQL 8.0 Command Line Client - Unicode**， 输入密码123456 进入到mysql

![image-20210207144549195](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207144549195-0KNpLi.png)

##  5. 软件安装目录

> **安装目录:** C:\Program Files\MySQL\MySQL Server 8.0
>
> **my.ini文件路径：**C:\ProgramData\MySQL\MySQL Server 8.0\my.ini
>
> **data目录**：C:\ProgramData\MySQL\MySQL Server 8.0\Data
>
> **安装程序的目录：**C:\Program Files (x86)\MySQL\MySQL Installer for Windows

## 6. MySQL Windows installer

这个很方便。

### 6.1 自定义安装（安装位置等）

![image-20210208175809307](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208175809307-UyggP0.png)

可以随意选择产品和版本。

![image-20210208175928651](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208175928651-RibVO5.png)

选中产品MySQL Server *，就会显示Advanced Options,点击可以自定义安装目录。

![image-20210208180540627](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208180540627-SEI4ga.png)

1. 程序安装目录
2. 数据目录和配置文件my.ini

![image-20210208180413811](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208180413811-IIMH6X.png)

选择功能

![image-20210208180607886](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208180607886-unlfxL.png)

接下来就和普通安装一样了

![image-20210208180644107](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208180644107-HH51wa.png)