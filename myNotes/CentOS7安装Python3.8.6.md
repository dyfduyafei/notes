<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210209124303327-2m6n1e.png" alt="image-20210209124303327" style="zoom: 33%;" />

Python: Python-3.8.6.tgz

CentOS: CentOS Linux release 7.8.2003 (Core)

## 1. 下载

wget下载

```shell
wget https://www.python.org/ftp/python/3.8.6/Python-3.8.6.tgz
```

## 2. 安装

解压源码包，进入解压目录

```shell
tar -zxvf Python-3.8.6.tgz
cd Python-3.8.6
```

提前安装依赖

```shell
yum install libffi-devel zlib openssl-devel
```

> 如果zlib 按出现问题 Multilib version problems found，用下面命令安装
>
> yum install -y zlib  --setopt=protected_multilib=false 

编译安装，会检查依赖，这里安装到 /usr/local/python3

```shell
./configure --prefix=/usr/local/python3
```

<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210209105030962-PTWBPV-Nws6jR.png" alt="image-20210209105030962" style="zoom:33%;" />

编译安装。

```
make && make install
```

稍等片刻，安装完成， pip也安装好了

<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210209120221139-gGoE7k.png" alt="image-20210209120221139" style="zoom: 25%;" />

## 3. 测试

进入目录，测试下python3 和 pip3

```shell
@default ➜ Python-3.8.6  cd /usr/local/python3/bin 
@default ➜ bin  ./python3
Python 3.8.6 (default, Feb  9 2021, 11:56:52) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-39)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> exit() 
@default ➜ bin  ./pip3 list
Package    Version
---------- -------
pip        20.2.1
setuptools 49.2.1
WARNING: You are using pip version 20.2.1; however, version 21.0.1 is available.
You should consider upgrading via the '/usr/local/python3/bin/python3.8 -m pip install --upgrade pip' command.
```

提示升级pip，按照提示命令升级。

```shell
@default ➜ bin  /usr/local/python3/bin/python3.8 -m pip install --upgrade pip
Collecting pip
  Downloading pip-21.0.1-py3-none-any.whl (1.5 MB)
     |████████████████████████████████| 1.5 MB 1.3 MB/s 
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 20.2.1
    Uninstalling pip-20.2.1:
      Successfully uninstalled pip-20.2.1
Successfully installed pip-21.0.1
@default ➜ bin  ./pip3 list
Package    Version
---------- -------
pip        21.0.1
setuptools 49.2.1
```

配置环境变量，打开系统配置文件

```shell
vim /etc/profile
```

添加环境变量

```shell
export PYTHON_HOME=/usr/local/python3
export PATH=$MYSQL_HOME/bin:$PYTHON_HOME/bin:$PATH
```
使配置生效
```shell
source /etc/profile
```

没问题

<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210209123737939-teBF3m.png" alt="image-20210209123737939" style="zoom:33%;" />

