## 1. 下载

官网推荐的安装方式

地址：https://www.python.org/ftp/python/3.9.1/python-3.9.1-amd64.exe

## 2. 安装

双击打开安装程序**python-3.9.1-amd64.exe**

勾选 Add Python 3.9 to Path, 程序自己配置环境变量

选择自定义安装 Customize installation

![image-20210210112812567](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210112812567-g5znuo.png)

来到功能选择界面，默认即可 Next

![image-20210210113009690](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210113009690-QM0Kg9.png)

高级选项界面， 自定义下安装路径（也可以不改），其他默认即可

> 如果勾选了 Install for all users , 环境变量就是系统级别的，否则就是用户级别的。

![image-20210210113920514](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210113920514-gwkvZH.png)

等待安装

![image-20210210114011847](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210114011847-BLtXm5.png)

安装完成，关闭界面。

![image-20210210114045265](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210114045265-3n0LNA.png)

用户的环境变量已经添加（高级选项中：我们没有勾选 Install for all users）

![image-20210210114915357](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210114915357-Zmp2ao.png)

## 3. 测试

命令行里测试，没问题，根据提示升级下pip版本

![image-20210210114733537](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210114733537-TcR8jP.png)

## 4. ipython

> `ipython`是一个`python`的交互式`shell`，比默认的`python shell`好用得多，支持变量自动补全，自动缩进，支持`bash shell`命令，内置了许多很有用的功能和函数。学习`ipython`将会让我们以一种更高的效率来使用`python`。同时它也是利用Python进行科学计算和交互可视化的一个最佳的平台。

```shell
pip install ipython
```

安装成功

![image-20210210121411309](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210121411309-9J7OrA.png)

测试下

![image-20210210121455840](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210121455840-fzgo3E.png)

## 5. jupyter

>Jupyter Notebook是基于网页的用于交互计算的应用程序。其可被应用于全过程计算：开发、文档编写、运行代码和展示结果。

```shell
pip install jupyter
```

安装完成

![image-20210210122432595](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210122432595-cqOq1m.png)

如何启动呢?

```shell
jupyter notebook
```

![image-20210210122616359](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210122616359-Iga8Mm.png)

会请求打开浏览器，可以看到jupyter默认工作目录是用户目录 C:\Users\duyafei\

![image-20210210122722135](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210122722135-ewGWH4.png)

如何自定义jupyter的默认工作目录呢？

```shell
jupyter notebook --generate-config
```

![image-20210210123204800](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210123204800-PU8U0p.png)

生成了一个配置文件  C:\Users\duyafei\.jupyter\jupyter_notebook_config.py， 打开它，全文搜索

notebook_dir， 这个样子的

![image-20210210123634351](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210123634351-iNTQ6p.png)

去掉# ，修改 为自己的工作目录，比如  C:\IDE\workSpace   记得保存

![image-20210210123824559](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210123824559-nXq3Eg.png)

启动jupyter， 创建一个文件看下

![image-20210210124041271](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210124041271-L2Y8nr.png)

看看window 目录， 工作目录改变了。

<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210210124119913-UcaLW6.png" alt="image-20210210124119913" style="zoom: 67%;" />

