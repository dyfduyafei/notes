[TOC]

## Gitee配置

### 创建仓库

![image-20210206121243200](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210206121243200-RCB47v.png)

### 生成私人令牌

![image-20210206121201300](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210206121201300-okayJD.png)

## uPic 配置

### 安装

uPic地址:  https://gitee.com/gee1k/uPic  有详细的使用说明

下载安装，记得系统授权

### 图床配置

![image-20210206110123264](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210206110123264-ZnI2JS.png)

### 验证

> 验证上传成功后，可以在在gitee仓库里看到图片

![09E88FF9-D9AB-4337-AEF5-56889133529F-14754-00006422761DE033](https://gitee.com/dyfSama/pic-bed/raw/master/md/09E88FF9-D9AB-4337-AEF5-56889133529F-14754-00006422761DE033-3LM7rk.gif)

##Typora配置

### 图像上传服务配置

支持多个服务

![image-20210206111024061](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210206111024061-ZyoY7N-pZCjuo.png)

![image-20210206123116559](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210206123116559-0fyt0E.png)

> 配置完成之后，无需打开uPic就可以实现上传的

### 演示

可以看到插入图片时会自动上传：图片url由本地路径变为gitee路径

![0A485FB0-E905-468C-8A7C-74342C12B294-46781-00006852F1A0F4A9](https://gitee.com/dyfSama/pic-bed/raw/master/md/0A485FB0-E905-468C-8A7C-74342C12B294-46781-00006852F1A0F4A9-S4pfPv.gif)



