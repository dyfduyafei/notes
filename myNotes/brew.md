#### brew  配置

[TOC]



###### 查看配置信息

```shell
brew config

# 显示
HOMEBREW_VERSION: 2.2.10
ORIGIN: https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/brew.git
HEAD: 48a1637b24a0c329905a981f4cff547c7fa47943
Last commit: 3 months ago
Core tap ORIGIN: https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-core.git
Core tap HEAD: 57bb88865299fbc0377d50823f2be73000833b16
Core tap last commit: 4 days ago
HOMEBREW_PREFIX: /usr/local
CPU: dodeca-core 64-bit kabylake
Homebrew Ruby: 2.6.3 => /System/Library/Frameworks/Ruby.framework/Versions/2.6/usr/bin/ruby
Clang: 11.0 build 1103
Git: 2.21.0 => /usr/local/bin/git
Curl: 7.64.1 => /usr/bin/curl
Java: 14, 11.0.2, 1.8.0_201
macOS: 10.15.5-x86_64
CLT: 1103.0.32.29
Xcode: N/A
```

###### 更改源

[清华源](https://mirror.tuna.tsinghua.edu.cn/help/homebrew/)

```shell
# 切换brew
git -C "$(brew --repo)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/brew.git

# 切换homebrew-core
git -C "$(brew --repo homebrew/core)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-core.git

# 切换homebrew-cask
git -C "$(brew --repo homebrew/cask)" remote set-url origin https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-cask.git
```

###### 切换Homebrew-bottles

```shell
# bash
echo 'export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.tuna.tsinghua.edu.cn/homebrew-bottles' >> ~/.bash_profile
source ~/.bash_profile

# zsh
echo 'export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.tuna.tsinghua.edu.cn/homebrew-bottles' >> ~/.zshrc
source ~/.zshrc
```

###### 恢复源

```shell
# 恢复 brew
git -C "$(brew --repo)" remote set-url origin https://github.com/Homebrew/brew.git

# 切换homebrew-core
git -C "$(brew --repo homebrew/core)" remote set-url origin https://github.com/Homebrew/homebrew-core.git

# 切换homebrew-cask
git -C "$(brew --repo homebrew/cask)" remote set-url origin https://github.com/Homebrew/homebrew-cask.git
```

#### brew命令

[homebrew](https://brew.sh/)

```shell
# 搜索
brew search xxx

# 安装
brew install xxx

# 查询 主要查看具体的信息及依赖关系当前版本注意事项
brew info xxx

# 如果想要更新到当前最新的版本要先把当前 brew 更新到最新。
brew update 

#列出所有有新版本的程序
brew outdated

# 升级所有
brew upgrade

# 指定程序升级
brew upgrade xxx

# 清理：清理不需要的版本及其安装缓存
brew cleanup

# 删除不需要的程序
brew uninstall xxx

# 参考文档
man brew
```



#### brew services命令【管理服务】

[brew services](https://docs.brew.sh/Manpage#services-subcommand)

```shell
# 列出brew管理的服务
brew services list 

@MBP.dyf ➜ ~  brew services list
Name              Status  User    Plist
kafka             stopped
mongodb-community stopped
mysql             started duyafei /Users/duyafei/Library/LaunchAgents/homebrew.mxcl.mysql.plist
nginx             started duyafei /Users/duyafei/Library/LaunchAgents/homebrew.mxcl.nginx.plist
redis             started duyafei /Users/duyafei/Library/LaunchAgents/homebrew.mxcl.redis.plist
zookeeper         stopped

# 运行服务（仅启动不注册）
brew services run xxx 
# 启动服务，并注册,
brew services start xxx  
# 停止服务，并取消注册
brew services stop xxx   
# 重启服务，并注册
brew services restart xxx 
# 清除无用的服务
brew services cleanup  
```

