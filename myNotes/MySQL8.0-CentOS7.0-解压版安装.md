<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/mysql-centos_banner-f7BhFQ.jpg" alt="&quot;centosmysql&quot;" style="zoom: 67%;" />

MySQL: mysql-8.0.23-el7-x86_64.tar

CentOS: CentOS Linux release 7.8.2003 (Core)

## 1. 下载

网址: https://dev.mysql.com/downloads/mysql/

<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208210527226-QurfNY.png" alt="image-20210208210527226" style="zoom:50%;" />

![image-20210208210544297](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208210544297-jQqTZn.png)

## 2. 安装

解压到/usr/local，然后修改文件夹名字为 mysql

```shell
tar -zxvf mysql-8.0.23-el7-x86_64.tar.gz -C /usr/local
mv mysql-8.0.23-el7-x86_64  mysql
```

![image-20210208212121776](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208212121776-FKGy0O.png)

添加mysql用户和用户组

```shell
groupadd mysql
useradd -r -g mysql mysql
```

创建数据文件夹

```shell
cd mysql && mkdir data
```

开始安装，user是我们新创建的mysql，basedir是安装的根目录，datadir是数据文件目录，我们之前创建的data

```shell
./bin/mysqld --initialize --user=mysql --basedir=/usr/local/mysql/ --datadir=/usr/local/mysql/data
```

安装成功，记住临时密码：**s0dKhrGanT:l**

![image-20210208213743041](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208213743041-TV2JNZ.png)

## 3. 启动和基本配置

启动MySQL服务

```shell
./support-files/mysql.server start
```

![image-20210208214716363](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208214716363-IaT0mP.png)

启动成功了，用前面的临时密码登录mysql，并尝试使用数据库，提示错误1820

```
./bin/mysql -uroot -ps0dKhrGanT:l
```

![image-20210208214901981](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208214901981-G2Yhbx.png)

需要修改密码,输入一下命令，这里会进行强密码校验，所以我的密码是 **1qaz@WSX**，修改后可以使用数据库了。

```shell
set password='1qaz@WSX';
```

![image-20210208215312478](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208215312478-OQHdLl.png)

惯例，配置远程访问,修改root用户的host，并刷新权限

```mysql
update user set host='%' where user = 'root';
flush privileges;
```

![image-20210208220159741](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208220159741-1RrLiv.png)

远程访问下，没问题。

![image-20210208220409811](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208220409811-oNCpvj.png)

## 4. 添加到系统服务

```shell
cp support-files/mysql.server /etc/init.d/mysql
```

使用没问题

![image-20210208222340707](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208222340707-EnqGEI.png)

设置开机启动MySQL服务

```shell
 chkconfig --add mysql
```

## 5. 添加MySQL环境变量

配置环境变量，打开系统配置文件

```shell
vim /etc/profile
```

添加环境变量

```shell
export MYSQL_HOME=/usr/local/mysql
export PATH=$MYSQL_HOME/bin:$PATH
```

使配置生效

```shell
source /etc/profile
```

环境变量没问题

![image-20210208224043264](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210208224043264-fmz9Gg.png)

