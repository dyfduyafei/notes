[TOC]



## 注册坚果云账号-添加应用

### 注册账号

坚果云 https://www.jianguoyun.com/d/signup

### 添加应用

右上角用户名-账户信息-安全选项

点击添加应用，应用名称为Floccus，如图为创建结果

![Snipaste_2021-02-05_10-51-54](https://gitee.com/dyfSama/pic-bed/raw/master/md/Snipaste_2021-02-05_10-51-54.jpg)

## 安装浏览器插件Floocus

### edge直接安装

安装完成如图

![image-20210205154114483](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205154114483.png)

### chrome翻墙安装（可以用google访问助手）

## 配置edge上的FLoocus

### 添加及配置账户

1. 点击图标，点击添加账户，然后选择第二个

   ![image-20210205154331028](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205154331028.png)

2. 录入坚果云中的：服务器地址、账户、密码、书签路径

   ![image-20210205154555792](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205154555792.png)

3. 选择浏览器书签文件夹，该文件文件夹下会全部同步

   这里选择的书签栏的

   ![image-20210205154641370](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205154641370.png)

4. 同步频率，默认15分钟

   ![image-20210205154754922](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205154754922.png)

5. 最终如图有一个账户，点击同步，坚果云会生成文件“bookmarks.xbel” ，打开里面其实就是书签信息

   ![image-20210205155049514](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205155049514.png)

   ![image-20210205155214582](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205155214582.png)

### Floocus账户信息从edge复制到chrome

把edge的账户信息导出成文件，然后倒入到chrome的floocus中

1. 导出

   ![image-20210205155440646](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205155440646.png)

   ![image-20210205155507497](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205155507497.png)

   会导出一个json文件

   ![image-20210205155758091](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205155758091.png)

2. 导入这个json文件

   chrome中同样的地方点击导入，就有同样的账户了

   ![image-20210205155605637](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205155605637.png)

   ![image-20210205155907023](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210205155907023.png)

## 体验同步效果

​		接下来就可以自动同步了，当然也可以手动了

​  其实就是 通过书签文件bookmarks.xbel 来传递书签信息。