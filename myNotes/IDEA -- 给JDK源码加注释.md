默认IDEA中的 JDK源码是只读的。

![image-20210207181554641](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207181554641-Nv4vMH.png)

那是因为关联的源码是在压缩包里：src.zip

![image-20210207181902303](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207181902303-FiQexb.png)

所以我们要自己配置IDEA关联的源码

### mac下的操作

1. IDEA中   快捷键  **⌘ + ;**  打开**Project Structure**,找到**Platfrom Settings - SDKs-SourcePath**,可以看到关联的确实是src.zip

   ![image-20210207182519704](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207182519704-KHznxI-tqt2GW.png)

2. 找到该目录，解压源码，然后修改这个关联。

   ```shell
   cd $JAVA_HOME
   ```

   创建文件夹src并解压到src, 需要sudo，

   ```shell
   sudo mkdir src  
   sudo cp src.zip src
   cd src
   sudo unzip src.zip
   ```

   还要赋权限，这里src 属于root

   ```shell
   sudo chmod -R 777 src
   ```

   源码有了

   ![](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207183807165-sVb8L1.png)

3. 配置IDEA，删除原来的关联，添加自己的源码路径，保存配置

![image-20210207184654271](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207184654271-5EXwji-InxPH9.png)

### 测试

尝试编辑时，选择整个目录，确认。

![image-20210207185024921](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207185024921-2kUu9I.png)

确实可以了

![image-20210207185057974](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210207185057974-0vJMDq.png)

>这里加注释，最好行尾注释，不要改变jdk源码的原有行数。