---
typora-copy-images-to: upload
---

# linux服务器免密码登录

## 1.linux下生成秘钥

####ssh-keygen -t rsa

```shell
@default ➜ ~  ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Created directory '/root/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:XXcSsHNjeEG6HxRL4C1X0Fo1WhxTedZelP7f4ZAGMPA root@default
The key's randomart image is:
+---[RSA 2048]----+
|       ..   o+OO%|
|        .o . *oXX|
|         Eo O.&=+|
|         . o @.=.|
|        S . o o .|
|             = o.|
|            . + +|
|               .o|
|                 |
+----[SHA256]-----+
```

#### 		生成两个文件 id_rsa和id_rsa.pub

```shell
@default ➜ ~  cd .ssh 
@default ➜ .ssh  ll
总用量 8.0K
-rw-------. 1 root root 1.7K 1月   6 11:45 id_rsa
-rw-r--r--. 1 root root  394 1月   6 11:45 id_rsa.pub
```

#### 查看下id_rsa.pub的内容

```shell
@default ➜ .ssh  cat id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCamZj1xxoT0dc5aUAJ3cxmOmgtovlfi6PNoqlY371AP4f3ckksEm5Yuffge8BoqQsX2uYwrh6APRu4ZVSMqtg3aKbeLp99mKP6tX7+Plscz67T29T9MkkfbTG3x6S7/H0+aTyGDg4wLhls20mTdKHrki+Gq8CYzOIrYuto2amfXEEMDzMQvODS+VW+aXozOK4NhCkgtRCNMOvrwRdpJH8ixpiJZXrweVJQZ5DxVfEw2vDKMEau1PSp23Jp33CL/ImXCIsi65StBZbEFux5ot05CYaacze4Ddy1ewrzcCWJuqfU0Sl2qSRANMSUt3NtmdUKlH/sbjuExxdLA3i8tRVz root@default
```

#### 查看下id_rsa的内容

```shell
@default ➜ .ssh  cat id_rsa
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAmpmY9ccaE9HXOWlACd3MZjpoLaL5X4ujzaKpWN+9QD+H93JJ
LBJuWLn34HvAaKkLF9rmMK4egD0buGVUjKrYN2im3i6ffZij+rV+/j5bHM+u09vU
/TJJH20xt8eku/x9Pmk8hg4OMC4ZbNtJk3Sh65IvhqvAmMziK2LraNmpn1xBDA8z
ELzg0vlVvml6MziuDYQpILUQjTDr68EXaSR/IsaYiWV68HlSUGeQ8VXxMNrwyjBG
rtT0qdtyad9wi/yJlwiLIuuUrQWWxBbseaLdOQmGmnM3uA3ctXsK83Alibqn1NEp
dqkkQDTElLdzbZnVCpR/7G47hMcXSwN4vLUVcwIDAQABAoIBAH6CZF3zMI65b3KG
gyXPv1yEPQ3jSEd8YG18xzF33Uj+9Ad0GRacennWrFWhTuEWO4Ko2SdKxKDR4KYz
HU4C2+3zkGFOK6s+Ril5bdMlOa/I71pkkNUk2huCYmXuVAqU4fQ5b5KPW+LnRl0C
0SF+FqZLuOJuF6uyNP2l89eYDirdsKEdHTUQFyk1FQn6yxxvTcgtAQbNTtT+94aj
B2QyZSTusp6yBabLH94kFYm1sZYJTRs9tBRsZhF2P3LRkb/5Fo/L+v9QSeI/f7vB
8mOiAwWPGl8HxhO0wWkZs7wyoYs4fCoDLPDX0qa/3bANKoc50x9kSnAbBXi0pgfQ
J9A5QMkCgYEAyuQKwPbGlVu91dYPOetKpNx6rHNGdS7ZJw7fB7qOy26LkPMsnGzy
E29tHAo37NhfNxi6I3MFiaTOwef1D1BU6lw8P1kudIyrMkxrIsDco86SKMs1yCgV
S3nVu09o6sptZUsN4thJPlTsbEocDBTYbfEjnzQp+GyPRbSslLo+p00CgYEAwxGH
zC6WTXvJA8WBq6fj58nwca9YOvXyUb3H40hALrPCPgDUzMV8PoY5Xl4v73esk7oH
59TFakaVfWu38J4w3kAeMRt6vS2ODAbkm0+I9l0zNJgcFlyeOQmuO86ilKhU1qTv
2fLxlsCDllB75UVOcKLIfMKSDbzAcX+gRDfrz78CgYEAvChkOLQjUlx0Xx1XnZUx
8lZuhgOZ8g7yYCCQgfBngQ4R7Ok4FBGNJq0NeRWY69N16fjKlxmSpyXqgTWGtR8A
wR+s1+rzBC94jPsF2IMXm+p07dQXGnrh1M82gbGRUT1N2sSSKi//LQlBAORxwlqK
pNse+AQ+cB7td+2op31ZoXkCgYB8ZTSFR/w/gz3oMs6DafhTexrjVJ9eUjNqXy0W
Sp/raGTpZ1xNDW8y7COvgz7sZhPezRZ3h98w67wvFD9jqW2efaMDS/PUqjVYhBjK
1kiQW1TpKEtZE00vMHY024wgYsxfaSUvhtb7fN8tPzwTNERWXeiebvH24rSSbIIG
nua5PwKBgQCqXp4lrVmjCIPXJTi5m/zVrUie9Q9syn2/0/hwLl7Eg6RgwzCjxdNN
oUX0lgVBSFF+u0LoH4qxjAMt9qYjjcYKev6CGXDhsPklWGCBewfMNBI/p2oRzfqW
yVVUJe6XFe2MQVLRHtbQyEfyPkQyLxmUEljF8CowGS1FPEFvFmFMkw==
-----END RSA PRIVATE KEY-----
```

![image-20210113103645123](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210113103645123.png)

## 2.远程免密登录

#### 原理

简单的说就是A生成公钥后把公钥的串（字符串）添加到B的authorized_keys文件中。

另外要注意的是权限问题。.ssh目录要700,authorized_keys文件要600。也有说是755和655点，反正后面两个不能有写权限。

<img src="https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210113103645504.png" alt="image-20210113103645504" style="zoom:80%;" />

##### 方法一

```shell
ssh-copy-id -i ~/.ssh/id_rsa.pub   用户名@ip地址
```

##### 方法二

```shell
先    scp ~/.ssh/id_dsa.pub 用户名@IP地址:/tmp 
然后   cat /tmp/id_dsa.pub >> ~/.ssh/authorized_keys
```

