### **该方法适用JetBrains 系列产品**

以IDEA为例

刚打开是这样的

![image-20210211001034756](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211001034756-HW6T3c.png)

勾选试用 Evaluate for free，点击 使用 Evaluate

![image-20210211001201383](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211001201383-OwWbOY.png)

然后就进入软件，选择Plugins - 螺丝帽图标- Manage Plugin Repositories 

![image-20210211001418911](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211001418911-RX9nwr.png)

打开插件仓库，添加仓库地址 https://plugins.zhile.io  ok保存信息。

![image-20210211001655787](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211001655787-FSvkvm.png)

在插件市场中搜索 eval，然后安装 IDE Eval Reset 这个插件（接受第三方仓库）

![image-20210211001901018](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211001901018-CMZRzG.png)

重启，然后左下角菜单会增加一个Eval Reset，打开它

![image-20210211002122064](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211002122064-pzZHIW.png)

试用重置的窗口

![image-20210211002227503](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211002227503-6h1OWw.png)

勾选 Auto reset before per restart (每次重启自动重置试用时间)，然后点击 Reset，提示重启 ，重启idea

![image-20210211002506901](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211002506901-FJ0CAn.png)

重启之后，再次打开 Eval Reset ，可以看到，试用期截止到 2010-03-13，之后每次重启都会重置试用期，就相当于无限试用了。

![image-20210211002606691](https://gitee.com/dyfSama/pic-bed/raw/master/md/image-20210211002606691-b3cTCD.png)

### 感谢插件提供者

https://zhile.io/2020/11/18/jetbrains-eval-reset.html